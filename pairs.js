function pairs(object) {
  if (typeof object !== "object" || Array.isArray(object)) {
    return [];
  }
  let objArray = [];

  for (const key in object) {
    const pair = [key, object[key]];
    objArray.push(pair);
  }

  return objArray;
}

module.exports = pairs;
