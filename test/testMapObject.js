const mapObject = require("../mapObject");

let result = mapObject(
  { name: "Bruce Wayne", age: 36, location: "Gotham" },
  function (value) {
    if (typeof value === "string") {
      return value.toUpperCase();
    }

    return value;
  }
);

console.log(result);
