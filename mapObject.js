function mapObject(object, callback) {
  if (
    typeof object !== "object" ||
    Array.isArray(object) ||
    typeof callback !== "function"
  ) {
    return {};
  }

  let newObject = {};

  for (const key in object) {
    let value = callback(object[key]);
    newObject[key] = value;
  }

  return newObject;
}

module.exports = mapObject;
