function keys(object) {
  if (typeof object !== "object") {
    return [];
  }

  const objKeys = [];

  for (const key in object) {
    objKeys.push(key);
  }

  return objKeys;
}

module.exports = keys;
