function values(object) {
  if (typeof object !== "object" || Array.isArray(object)) {
    return [];
  }

  const objValues = [];

  for (const key in object) {
    const value = object[key];
    if (typeof value !== "function") {
      objValues.push(value);
    }
  }

  return objValues;
}

module.exports = values;
