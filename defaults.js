function defaults(object, defaultProps) {
  if (
    typeof object !== "object" ||
    Array.isArray(object) ||
    typeof defaultProps !== "object" ||
    Array.isArray(defaultProps)
  ) {
    return [];
  }

  for (const key in defaultProps) {
    if (object[key] === undefined) {
      object[key] = defaultProps[key];
    }
  }

  return object;
}

module.exports = defaults;
