function invert(object) {
  if (typeof object !== "object" || Array.isArray(object)) {
    return [];
  }

  const invertedObject = {};

  for (const key in object) {
    invertedObject[object[key]] = key;
  }

  return invertedObject;
}

module.exports = invert;
